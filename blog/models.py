from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.urls import reverse


from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField


class Subject(models.Model):
	title  = models.CharField('Tiêu đề', max_length = 200)
	slug = models.CharField(verbose_name = 'URL', max_length= 200, unique = True)	
	overview = models.TextField('Giới thiệu',)
	
	created = models.DateTimeField(('Ngày tạo'), auto_now_add = True)

	class Meta:
		ordering = ['title']
		verbose_name = 'Chủ đề'
		verbose_name_plural = 'Các chủ đề'

	
	def __str__(self):
		return self.title


	def save(self, *args, **kwargs):

		self.slug=  slugify(self.title, allow_unicode = True)
		super(Subject,self).save()


class Course(models.Model):
	owner = models.ForeignKey(settings.AUTH_USER_MODEL,
							related_name = 'courses_created',
							on_delete = models.CASCADE)
	subject = models.ForeignKey(Subject,
							related_name = 'courses',
							on_delete = models.CASCADE)
	title = models.CharField(verbose_name = 'Tiêu đề', max_length= 200)
	slug = models.CharField(verbose_name = 'URL', max_length = 200, unique= True)
	image = models.ImageField(('Hình ảnh'),upload_to = 'images/%Y/%m/%d/', blank = True, null = True)
	overview = models.TextField('Giới thiệu',)
	created=  models.DateTimeField(('Ngày tạo'),auto_now_add= True)

	class Meta:
		ordering = ['-created']
		verbose_name = 'Bài viết'
		verbose_name_plural = 'Các bài viết'

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('post_detail', args = [self.slug])


	def save(self, *args, **kwargs):

		self.slug=  slugify(self.title, allow_unicode = True)
		super(Course,self).save()

class Module(models.Model):
	course = models.ForeignKey(Course,
							verbose_name= 'Nội dung bài',
							related_name= 'modules',
							on_delete = models.CASCADE)

	title = models.CharField(verbose_name= 'Tiêu đề', max_length =200, null=True,blank= True)
	
	content =  RichTextUploadingField(
		('Nội dung'),
		# config_name = 'special',
		external_plugin_resources =[(
			'youtube',
			'/static/ckeditor_plugins/youtube/youtube/',
			'plugin.js',
		)],
	)

	# amodule = models.ForeignKey('Module',
	# 						null = True,
	# 						on_delete= models.CASCADE,
							
	# 						)

	def __str__(self):
		return self.title

