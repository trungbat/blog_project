from django.shortcuts import render,  get_object_or_404
from .models import Course, Subject

def ListPostView(request):
	obj = Course.objects.all()
	subj = Subject.objects.all()[:5]

	content = {'posts':obj,'section':'home', 'subj':subj}
	
	return render(request, 'blog/list_post.html', content)

def PostDetailView(request,slug):
	post = get_object_or_404(Course, slug = slug )
	subj = Subject.objects.all()[:5]

	modules = post.modules.all()

	content = {'post':post,'modules':modules, 'section':'noidung', 'subj':subj}

	return render(request, 'blog/detail_post.html', content)