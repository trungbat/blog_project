# Generated by Django 2.1.2 on 2018-10-14 17:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0010_module_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='module',
            name='image',
        ),
        migrations.AddField(
            model_name='course',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='images/%Y/%m/%d/', verbose_name='Hình ảnh'),
        ),
    ]
