from django.urls import path
from . import views


urlpatterns = [
   path('', views.ListPostView, name = 'list_post'),
   path('blog/<str:slug>/', views.PostDetailView, name= 'post_detail'),
]